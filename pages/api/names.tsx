// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import { NextApiRequest, NextApiResponse } from 'next';
import { getNames } from '../../lib/name-data';
import { Gender, Query } from '../../lib/types';

export default (req: NextApiRequest, res: NextApiResponse) => {
  const query = {
    gender: req.query.gender ? (req.query.gender as Gender) : undefined,
    popularity: req.query.popularity
      ? JSON.parse(req.query.popularity as string)
      : undefined,
    maxLength: req.query.maxLength
      ? +req.query.maxLength
      : Number.MAX_SAFE_INTEGER,
    prefix: req.query.prefix,
  } as Query;
  const result = getNames(query);

  res.statusCode = 200;
  res.json(result);
};
