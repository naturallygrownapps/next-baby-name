import React from 'react';
import Popup from 'reactjs-popup';
import Head from 'next/head';
import { useEffect, useState } from 'react';
import useSWR from 'swr';
import classNames from 'classnames';
import { FavoriteNames } from '../components/FavoriteNames';
import { NameList } from '../components/NameList';
import {
  defaultSettings,
  hasSettings,
  loadSettings,
  saveSettings,
} from '../lib/storage';
import {
  CountryPopularity,
  Entry as NameEntry,
  Gender,
  Settings,
} from '../lib/types';
import styles from '../styles/Index.module.scss';
import { ConfigModal } from '../components/ConfigModal';
import shuffle from 'lodash/shuffle';
import { AppName } from '../lib/constants';
import { AboutModal } from '../components/AboutModal';
import { add } from 'lodash';

type SearchParams = {
  gender: Gender;
  maxLength?: number;
  countryFilter: string[];
  prefix: string;
};

type Tab = 'names' | 'favorites';

const fetcher = (...args) => (fetch as any)(...args).then((res) => res.json());

function useNames(searchParams: SearchParams) {
  let finalUrl = '/api/names';
  if (process.browser) {
    const url = new URL('/api/names', location.origin);
    url.searchParams.append('gender', searchParams.gender);
    if (searchParams.maxLength) {
      url.searchParams.append('maxLength', String(searchParams.maxLength));
    }
    if (searchParams.countryFilter && searchParams.countryFilter.length) {
      url.searchParams.append(
        'popularity',
        JSON.stringify(
          searchParams.countryFilter.reduce((result, cur) => {
            result[cur] = 1;
            if (cur === 'GERMANY') {
              result['EAST_FRISIA'] = 1;
            }
            return result;
          }, {} as CountryPopularity)
        )
      );
    }
    if (searchParams.prefix) {
      url.searchParams.append('prefix', searchParams.prefix);
    }

    finalUrl = url.toString();
  }
  const { data, error } = useSWR<NameEntry[]>(finalUrl, fetcher);

  console.log(`Found ${data && data.length} names.`);

  return {
    foundNames: data,
    isLoading: !error && !data,
    isError: error,
  };
}

export default function Home() {
  const [currentNames, setCurrentNames] = useState<string[]>([]);
  const [settings, setSettings] = useState<Settings>(defaultSettings);
  const [isConfigOpen, setIsConfigOpen] = useState(false);
  const [selectedTab, setSelectedTab] = useState<Tab>('names');
  const [listScrollOffset, setListScrollOffset] = useState(0);
  const {
    foundNames: foundNameResults,
    isError: namesLoadingError,
    isLoading: namesLoading,
  } = useNames(settings);

  useEffect(() => {
    setSettings(loadSettings());
    if (!hasSettings()) {
      setIsConfigOpen(true);
    }
  }, []);

  useEffect(() => {
    if (!foundNameResults) {
      setCurrentNames([]);
    } else {
      const namesOnly = foundNameResults.map((n) => n.name);
      const shuffledNames = settings.configuredNames.map((n) =>
        n.isRandom ? shuffle(namesOnly) : undefined
      );

      const generatedNames = [];
      for (let i = 0; i < namesOnly.length; i++) {
        generatedNames.push(
          settings.configuredNames.reduce((prev, cur, index) => {
            let n = cur.isRandom ? shuffledNames[index][i] : cur.name;
            return prev + ' ' + n;
          }, '')
        );
      }

      setCurrentNames(generatedNames);
    }
  }, [foundNameResults, settings.configuredNames]);

  const updateSettings = (newSettings: Settings) => {
    saveSettings(newSettings);
    setSettings(newSettings);
  };

  const addFavorite = (name: string) => {
    const newFavorites = [...settings.favorites, name];
    updateSettings({
      ...settings,
      favorites: newFavorites,
    });
  };

  const removeFavorite = (name: string) => {
    const newFavorites = settings.favorites.filter((n) => n !== name);
    updateSettings({
      ...settings,
      favorites: newFavorites,
    });
  };

  const toggleFavorite = (name: string) => {
    (settings.favorites.indexOf(name) > -1 ? removeFavorite : addFavorite)(
      name
    );
  };

  const isFavorite = (name: string) => {
    const favorites = settings?.favorites;
    return favorites && favorites.indexOf(name) > -1;
  };

  return (
    <div className={styles.container}>
      <Head>
        <title>{AppName}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          {AppName}
          <div className={styles.settingsContainer}>
            <Popup
              trigger={
                <button className={styles.btnConfig}>
                  <img src="/info-circle-solid.svg" />
                </button>
              }
              modal
            >
              {(close: () => void) => <AboutModal close={close} />}
            </Popup>
            <Popup
              open={isConfigOpen}
              onClose={() => setIsConfigOpen(false)}
              trigger={
                <button className={styles.btnConfig}>
                  <img src="/cog-solid.svg" />
                </button>
              }
              modal
            >
              {(close: () => void) => (
                <ConfigModal
                  settings={settings}
                  close={(newSettings) => {
                    if (newSettings) {
                      updateSettings(newSettings);
                    }
                    close();
                  }}
                />
              )}
            </Popup>
          </div>
        </h1>

        {namesLoadingError && (
          <div>ERROR: {JSON.stringify(namesLoadingError)}</div>
        )}

        {namesLoading ? (
          <div className={styles.loading}>
            <img src="/spinner-solid.svg" />
          </div>
        ) : (
          <>
            <div className={styles.tabButtonsContainer}>
              <button
                className={classNames(styles.btnTab, 'button', {
                  [styles.isSelected]: selectedTab === 'names',
                })}
                onClick={() => setSelectedTab('names')}
              >
                <img src="/dice-solid.svg" /> Namen
              </button>
              <button
                className={classNames(styles.btnTab, 'button', {
                  [styles.isSelected]: selectedTab === 'favorites',
                })}
                onClick={() => setSelectedTab('favorites')}
              >
                <img src="/heart.svg" /> Favoriten
              </button>
            </div>
            {selectedTab === 'names' && (
              <NameList
                names={currentNames}
                onMarkFavorite={toggleFavorite}
                isFavorite={isFavorite}
                onScroll={setListScrollOffset}
                initialScrollOffset={listScrollOffset}
              />
            )}
            {selectedTab === 'favorites' && (
              <FavoriteNames
                favoriteNames={settings.favorites}
                onRemoveFavorite={removeFavorite}
              />
            )}
          </>
        )}
      </main>
    </div>
  );
}
