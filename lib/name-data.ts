import fs from 'fs';
import path from 'path';
import { Entry, Gender, Query } from './types';
import _ from 'lodash';

const genderMap: { [key: string]: Gender } = {
  M: 'male',
  '1M': 'male',
  '?M': 'male',

  F: 'female',
  '1F': 'female',
  '?F': 'female',

  '?': 'both',
};

function readEntries() {
  const e = JSON.parse(
    fs.readFileSync(path.join(process.cwd(), 'public', 'names.json'), 'utf-8')
  );
  for (const n of e) {
    const g = n.gender && n.gender.trim();
    n.gender = genderMap[g] ?? 'both';
  }
  return e as Entry[];
}

const entries = readEntries();

export function getNames(query: Query) {
  return _(entries)
    .filter((e) => !query.gender || e.gender == query.gender)
    .filter((e) => e.name.length <= query.maxLength)
    .filter((e) => !query.prefix || e.name.startsWith(query.prefix))
    .filter(
      (e) =>
        !query.popularity ||
        _.some(
          query.popularity,
          (v, k) => e.popularity[k] && e.popularity[k] >= v
        )
    )
    .take(10000)
    .value();
}
