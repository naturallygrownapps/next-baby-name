import { Settings } from './types';

const settingsStorageKey = 'settings';

export function loadSettings(): Settings {
  let settings: Settings;
  try {
    settings = JSON.parse(localStorage.getItem(settingsStorageKey));
  } catch {}

  if (!settings) {
    settings = defaultSettings;
  }

  return settings;
}

export function saveSettings(settings: Settings) {
  localStorage.setItem(settingsStorageKey, JSON.stringify(settings));
}

export function hasSettings() {
  return !!localStorage.getItem(settingsStorageKey);
}

export const defaultSettings = {
  configuredNames: [
    {
      isRandom: true,
    },
  ],
  favorites: [],
  gender: 'female',
  maxLength: 20,
  countryFilter: [],
  prefix: '',
} as Settings;
