export type Gender = 'male' | 'female' | 'both';

export const countryOffsets = {
  ANY_COUNTRY: 0,
  BRITAIN: 1,
  IRELAND: 2,
  USA: 3,
  SPAIN: 4,
  PORTUGAL: 5,
  ITALY: 6,
  MALTA: 7,
  FRANCE: 8,
  BELGIUM: 9,
  LUXEMBOURG: 10,
  NETHERLANDS: 11,
  GERMANY: 12,
  EAST_FRISIA: 13,
  AUSTRIA: 14,
  SWISS: 15,
  ICELAND: 16,
  DENMARK: 17,
  NORWAY: 18,
  SWEDEN: 19,
  FINLAND: 20,
  ESTONIA: 21,
  LATVIA: 22,
  LITHUANIA: 23,
  POLAND: 24,
  CZECH_REP: 25,
  SLOVAKIA: 26,
  HUNGARY: 27,
  ROMANIA: 28,
  BULGARIA: 29,
  BOSNIA: 30,
  CROATIA: 31,
  KOSOVO: 32,
  MACEDONIA: 33,
  MONTENEGRO: 34,
  SERBIA: 35,
  SLOVENIA: 36,
  ALBANIA: 37,
  GREECE: 38,
  RUSSIA: 39,
  BELARUS: 40,
  MOLDOVA: 41,
  UKRAINE: 42,
  ARMENIA: 43,
  AZERBAIJAN: 44,
  GEORGIA: 45,
  KAZAKH_UZBEK: 46,
  TURKEY: 47,
  ARABIA: 48,
  ISRAEL: 49,
  CHINA: 50,
  INDIA: 51,
  JAPAN: 52,
  KOREA: 53,
  VIETNAM: 54,
};

export type CountryPopularity = Partial<typeof countryOffsets>;

export type Entry = {
  gender: Gender;
  name: string;
  popularity: CountryPopularity;
};

export type Query = {
  gender: Gender;
  popularity: CountryPopularity;
  maxLength: number;
  prefix: string;
};

export type ConfiguredName = {
  name?: string;
  isRandom?: boolean;
};

export type Settings = {
  configuredNames: ConfiguredName[];
  favorites: string[];
  gender: Gender;
  maxLength: number;
  prefix: string;
  countryFilter: string[];
};
