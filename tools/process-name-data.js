const fs = require('fs');
const path = require('path');

const countryOffsets = {
  ANY_COUNTRY: 0,
  BRITAIN: 1,
  IRELAND: 2,
  USA: 3,
  SPAIN: 4,
  PORTUGAL: 5,
  ITALY: 6,
  MALTA: 7,
  FRANCE: 8,
  BELGIUM: 9,
  LUXEMBOURG: 10,
  NETHERLANDS: 11,
  GERMANY: 12,
  EAST_FRISIA: 13,
  AUSTRIA: 14,
  SWISS: 15,
  ICELAND: 16,
  DENMARK: 17,
  NORWAY: 18,
  SWEDEN: 19,
  FINLAND: 20,
  ESTONIA: 21,
  LATVIA: 22,
  LITHUANIA: 23,
  POLAND: 24,
  CZECH_REP: 25,
  SLOVAKIA: 26,
  HUNGARY: 27,
  ROMANIA: 28,
  BULGARIA: 29,
  BOSNIA: 30,
  CROATIA: 31,
  KOSOVO: 32,
  MACEDONIA: 33,
  MONTENEGRO: 34,
  SERBIA: 35,
  SLOVENIA: 36,
  ALBANIA: 37,
  GREECE: 38,
  RUSSIA: 39,
  BELARUS: 40,
  MOLDOVA: 41,
  UKRAINE: 42,
  ARMENIA: 43,
  AZERBAIJAN: 44,
  GEORGIA: 45,
  KAZAKH_UZBEK: 46,
  TURKEY: 47,
  ARABIA: 48,
  ISRAEL: 49,
  CHINA: 50,
  INDIA: 51,
  JAPAN: 52,
  KOREA: 53,
  VIETNAM: 54,
};

const specialCharMap = {
  '<A/>': 256,
  '<a/>': 257,
  '<Â>': 258,
  '<â>': 259,
  '<A,>': 260,
  '<a,>': 261,
  '<C´>': 262,
  '<c´>': 263,
  '<C^>': 268,
  '<CH>': 268,
  '<c^>': 269,
  '<ch>': 269,
  '<d´>': 271,
  '<Ð>': 272,
  '<DJ>': 272,
  '<ð>': 273,
  '<dj>': 273,
  '<E/>': 274,
  '<e/>': 275,
  '<E°>': 278,
  '<e°>': 279,
  '<E,>': 280,
  '<e,>': 281,
  '<Ê>': 282,
  '<ê>': 283,
  '<G^>': 286,
  '<g^>': 287,
  '<G,>': 290,
  '<g´>': 291,
  '<I/>': 298,
  '<i/>': 299,
  '<I°>': 304,
  '<i>': 305,
  '<IJ>': 306,
  '<ij>': 307,
  '<K,>': 310,
  '<k,>': 311,
  '<L,>': 315,
  '<l,>': 316,
  '<L´>': 317,
  '<l´>': 318,
  '<L/>': 321,
  '<l/>': 322,
  '<N,>': 325,
  '<n,>': 326,
  '<N^>': 327,
  '<n^>': 328,
  '<Ö>': 336,
  '<ö>': 337,
  Œ: 338,
  '<OE>': 338,
  œ: 339,
  '<oe>': 339,
  '<R^>': 344,
  '<r^>': 345,
  '<S,>': 350,
  '<s,>': 351,
  Š: 352,
  '<S^>': 352,
  '<SCH>': 352,
  '<SH>': 352,
  š: 353,
  '<s^>': 353,
  '<sch>': 353,
  '<sh>': 353,
  '<T,>': 354,
  '<t,>': 355,
  '<t´>': 357,
  '<U/>': 362,
  '<u/>': 363,
  '<U°>': 366,
  '<u°>': 367,
  '<U,>': 370,
  '<u,>': 371,
  '<Z°>': 379,
  '<z°>': 380,
  '<Z^>': 381,
  '<z^>': 382,
  '<ß>': 7838,
};

function escapeRegExp(string) {
  return string.replace(/[.*+\-?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}

function getName(entryLine) {
  let n = entryLine.substring(2, 29).trim().replace('+', ' ');
  for (const charReplace of Object.keys(specialCharMap)) {
    const r = new RegExp(escapeRegExp(charReplace), 'g');
    n = n.replace(r, String.fromCharCode(specialCharMap[charReplace]));
  }
  return n;
}

function getPopularity(entryLine) {
  const result = {};
  if (entryLine) {
    entryLine = entryLine.substring(30);

    for (const countryId of Object.keys(countryOffsets)) {
      const offset = countryOffsets[countryId];
      const offsetValue = entryLine[offset];
      if (offsetValue !== ' ') {
        const value = parseInt(offsetValue, 16);
        if (value) {
          result[countryId] = value;
        }
      }
    }
  }
  return result;
}

const names = fs
  .readFileSync(path.join(process.cwd(), 'data', 'nam_dict.txt'), 'utf-8')
  .split('\r\n')
  .filter((l) => l[0] !== '#')
  .map((l) => {
    return {
      gender: l.substring(0, 2),
      name: getName(l),
      popularity: getPopularity(l),
    };
  });

fs.writeFileSync(
  path.join(process.cwd(), 'public', 'names.json'),
  JSON.stringify(names),
  {
    encoding: 'utf-8',
  }
);
