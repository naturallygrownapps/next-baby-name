import styles from '../styles/About.module.scss';
import modalStyles from '../styles/ModalStyles.module.scss';
import { AppName } from '../lib/constants';

export function AboutModal({ close }: { close: () => void }) {
  return (
    <div className={modalStyles.modal}>
      <div className={modalStyles.header}>
        <h1>{AppName}</h1>
        <h2>About</h2>
      </div>

      <div className={modalStyles.content}>
        <p>A baby name finder using:</p>
        <p>
          Names from{' '}
          <a
            href="https://www.heise.de/ct/ftp/07/17/182/"
            target="_blank"
            rel="noopener noreferrer"
          >
            c't 40 000 Namen
          </a>
        </p>
        <p>
          Icons by{' '}
          <a
            href="https://fontawesome.com"
            target="_blank"
            rel="noopener noreferrer"
          >
            Font Awesome
          </a>
        </p>
      </div>
      <div className={modalStyles.actions}>
        <button onClick={() => close()} className="button">
          Schließen
        </button>
      </div>
    </div>
  );
}
