import styles from '../styles/NameSelection.module.scss';
import { FormEvent } from 'react';
import classNames from 'classnames';
import { ConfiguredName } from '../lib/types';

export default function NameSelection({
  names,
  setNames,
}: {
  names: ConfiguredName[];
  setNames: (newNames: ConfiguredName[]) => void;
}) {
  const toggleRandom = (index: number) => {
    setNames(
      names.map((n, i) => {
        if (i === index) {
          const isRandomNow = !n.isRandom;
          return {
            name: isRandomNow ? '' : n.name,
            isRandom: isRandomNow,
          };
        }
        return n;
      })
    );
  };

  const nameChanged = (index: number, e: FormEvent<HTMLInputElement>) => {
    setNames(
      names.map((n, i) => {
        if (i === index) {
          return {
            name: (e.target as HTMLInputElement).value,
            isRandom: n.isRandom,
          };
        }
        return n;
      })
    );
  };

  return (
    <div className={styles.nameSelection}>
      <div className={styles.description}>
        Stelle hier einen oder mehrere Namen ein. Namen können eingegeben werden
        (wenn du z.B. schon einen Teilnamen weißt oder deinen Nachnamen eingeben
        möchtest), oder generiert werden (Wüfel-Icon).
      </div>
      {!names || !names.length ? (
        <div className={styles.noNamesConfigured}>
          Noch kein Name konfiguriert. Wähle "+", um einen Namen hinzuzufügen.
        </div>
      ) : (
        names.map((n, i) => (
          <div key={i} className={styles.inputWithLabelContainer}>
            <label>{i + 1}. Name</label>
            <div className={styles.inputContainer}>
              {n.isRandom ? (
                <span className={styles.randomLabel}>wird generiert</span>
              ) : (
                <input
                  value={n.name}
                  onInput={(e) => nameChanged(i, e)}
                  onFocus={(e) => e.target.select()}
                  disabled={n.isRandom}
                  placeholder="Namen eingeben"
                />
              )}
              <button
                onClick={() => toggleRandom(i)}
                className={classNames(styles.diceButton, {
                  [styles.isToggled]: n.isRandom,
                })}
                title="Diesen Namen zufällig generieren"
              >
                <img src="/dice-solid.svg" />
              </button>
            </div>
          </div>
        ))
      )}
      <div className={styles.addRemoveButtonsContainer}>
        <button
          className={styles.addRemoveButton}
          onClick={() => setNames(names.slice(0, names.length - 1))}
        >
          <img src="/minus-circle-solid.svg" />
        </button>
        <button
          className={styles.addRemoveButton}
          onClick={() => setNames([...names, { isRandom: false }])}
        >
          <img src="/plus-circle-solid.svg" />
        </button>
      </div>
    </div>
  );
}
