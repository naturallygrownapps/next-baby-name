import styles from '../styles/FavoriteNames.module.scss';
import listStyles from '../styles/ListStyles.module.scss';
import classNames from 'classnames';

export function FavoriteNames({
  favoriteNames,
  onRemoveFavorite,
}: {
  favoriteNames: string[];
  onRemoveFavorite: (name: string) => void;
}) {
  if (!favoriteNames || !favoriteNames.length) {
    return <div className={styles.noFavorites}>Noch keine Namen gemerkt.</div>;
  }

  return (
    <div className={classNames(styles.favoriteNames, listStyles.container)}>
      {favoriteNames.map((n, i) => {
        return (
          <div className={classNames(styles.row, listStyles.row)} key={n + i}>
            <div>{n}</div>
            <div className={listStyles.buttons}>
              <button onClick={() => onRemoveFavorite(n)}>
                <img src="/minus-circle-solid.svg" />
              </button>
            </div>
          </div>
        );
      })}
    </div>
  );
}
