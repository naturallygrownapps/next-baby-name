import styles from '../styles/ConfigModal.module.scss';
import modalStyles from '../styles/ModalStyles.module.scss';
import React, { FormEvent, useEffect, useState } from 'react';
import Select from 'react-select';
import { ConfiguredName, Gender, Settings } from '../lib/types';
import NameSelection from './NameSelection';
import { AppName } from '../lib/constants';

const countries = {
  ALBANIA: 'Albania',
  ARABIA: 'Arabia',
  ARMENIA: 'Armenia',
  AUSTRIA: 'Austria',
  AZERBAIJAN: 'Azerbaijan',
  BELARUS: 'Belarus',
  BELGIUM: 'Belgium',
  BOSNIA: 'Bosnia',
  BULGARIA: 'Bulgaria',
  CHINA: 'China',
  CROATIA: 'Croatia',
  CZECH_REP: 'Czech Republic',
  DENMARK: 'Denmark',
  ESTONIA: 'Estonia',
  FINLAND: 'Finland',
  FRANCE: 'France',
  GEORGIA: 'Georgia',
  GERMANY: 'Germany',
  BRITAIN: 'Great Britain',
  GREECE: 'Greece',
  HUNGARY: 'Hungary',
  ICELAND: 'Iceland',
  INDIA: 'India',
  IRELAND: 'Ireland',
  ISRAEL: 'Israel',
  ITALY: 'Italy',
  JAPAN: 'Japan',
  KAZAKH_UZBEK: 'Kazakhstan/Uzbekistan',
  KOREA: 'Korea',
  KOSOVO: 'Kosovo',
  LATVIA: 'Latvia',
  LITHUANIA: 'Lithuania',
  LUXEMBOURG: 'Luxembourg',
  MACEDONIA: 'Macedonia',
  MALTA: 'Malta',
  MOLDOVA: 'Moldova',
  MONTENEGRO: 'Montenegro',
  NETHERLANDS: 'Netherlands',
  NORWAY: 'Norway',
  POLAND: 'Poland',
  PORTUGAL: 'Portugal',
  ROMANIA: 'Romania',
  RUSSIA: 'Russia',
  SERBIA: 'Serbia',
  SLOVAKIA: 'Slovakia',
  SLOVENIA: 'Slovenia',
  SPAIN: 'Spain',
  SWEDEN: 'Sweden',
  SWISS: 'Switzerland',
  TURKEY: 'Turkey',
  USA: 'USA',
  UKRAINE: 'Ukraine',
  VIETNAM: 'Vietnam',
  ANY_COUNTRY: 'Other Countries',
};

type CountryOptionType = {
  value: string;
  label: string;
};

const countryOptions = Object.keys(countries).map(
  (k) =>
    ({
      value: k,
      label: countries[k],
    } as CountryOptionType)
);

export function ConfigModal({
  close,
  settings: originalSettings,
}: {
  close: (newSettings: Settings) => void;
  settings: Settings;
}) {
  const [editedSettings, setEditedSettings] = useState<Settings>(
    originalSettings
  );
  const [selectedCountryOptions, setSelectedCountryOptions] = useState<
    CountryOptionType[]
  >([]);

  useEffect(() => {
    if (editedSettings.countryFilter) {
      setSelectedCountryOptions(
        editedSettings.countryFilter.map(
          (c) =>
            ({
              value: c,
              label: countries[c],
            } as CountryOptionType)
        )
      );
    } else {
      setSelectedCountryOptions([]);
    }
  }, [editedSettings.countryFilter]);

  const editSettings = (newSettings: Partial<Settings>) => {
    setEditedSettings({
      ...editedSettings,
      ...newSettings,
    });
  };

  const updateGender = (e: FormEvent<HTMLInputElement>) => {
    editSettings({
      gender: (e.target as HTMLInputElement).value as Gender,
    });
  };

  const updateMaxLength = (e: FormEvent<HTMLInputElement>) => {
    editSettings({
      maxLength: Number((e.target as HTMLInputElement).value),
    });
  };

  const updateNameConfigs = (newNames: ConfiguredName[]) => {
    editSettings({
      configuredNames: newNames,
    });
  };

  const updateCountryFilter = (newSelection: CountryOptionType[]) => {
    newSelection = Array.isArray(newSelection) ? newSelection : [];
    if (newSelection) {
      editSettings({
        countryFilter: newSelection.map((i) => i.value),
      });
    }
  };

  const updatePrefix = (e: FormEvent<HTMLInputElement>) => {
    editSettings({
      prefix: (e.target as HTMLInputElement).value,
    });
  };

  return (
    <div className={modalStyles.modal}>
      <div className={modalStyles.header}>
        <h1>{AppName}</h1>
        <h2>Einstellungen</h2>
      </div>
      <div className={modalStyles.content}>
        <div className={styles.genderSelection}>
          <span className={styles.genderRadioContainer}>
            <input
              name="gender"
              id="gender-female"
              type="radio"
              value="female"
              checked={editedSettings.gender === 'female'}
              onChange={updateGender}
            />
            <label htmlFor="gender-female">Mädchennamen</label>
          </span>
          <span className={styles.genderRadioContainer}>
            <input
              name="gender"
              id="gender-male"
              type="radio"
              value="male"
              checked={editedSettings.gender === 'male'}
              onChange={updateGender}
            />
            <label htmlFor="gender-male">Jungennamen</label>
          </span>
          <span className={styles.genderRadioContainer}>
            <input
              name="gender"
              id="gender-both"
              type="radio"
              value="both"
              checked={editedSettings.gender === 'both'}
              onChange={updateGender}
            />
            <label htmlFor="gender-both">Egal</label>
          </span>
        </div>
        <div className={styles.maxLengthContainer}>
          <label htmlFor="inputMaxLength">Maximale Länge</label>
          <input
            id="inputMaxLength"
            min="1"
            max="50"
            type="number"
            value={editedSettings.maxLength}
            onChange={updateMaxLength}
          />
        </div>
        <div className={styles.nameSelectionContainer}>
          <NameSelection
            names={editedSettings.configuredNames}
            setNames={updateNameConfigs}
          />
        </div>
        <div className={styles.countrySelectionContainer}>
          <div className={styles.description}>
            Schränke die Namen auf solche ein, die in den folgenden Ländern
            gebräuchlich sind (optional):
          </div>
          <Select
            options={countryOptions}
            isMulti
            placeholder="Länder/Regionen auswählen"
            value={selectedCountryOptions}
            onChange={updateCountryFilter}
          />
        </div>
        <div className={styles.prefixSelectionContainer}>
          <label htmlFor="inputPrefix">Nur Namen mit diesem Präfix</label>
          <input
            id="inputPrefix"
            value={editedSettings.prefix}
            onChange={updatePrefix}
          />
        </div>
      </div>
      <div className={modalStyles.actions}>
        <button onClick={() => close(null)} className="button">
          Abbrechen
        </button>
        <button
          className="button"
          onClick={() => {
            close(editedSettings);
          }}
        >
          Namen anzeigen
        </button>
      </div>
    </div>
  );
}
