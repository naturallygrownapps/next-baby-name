import styles from '../styles/NameList.module.scss';
import listStyles from '../styles/ListStyles.module.scss';
import classNames from 'classnames';
import { FixedSizeList as List, ListOnScrollProps } from 'react-window';
import AutoSizer from 'react-virtualized-auto-sizer';
import { CSSProperties, useState } from 'react';

export function NameList({
  names,
  onMarkFavorite,
  isFavorite,
  initialScrollOffset,
  onScroll,
}: {
  names: string[];
  onMarkFavorite: (name: string) => void;
  isFavorite: (name: string) => boolean;
  initialScrollOffset: number;
  onScroll: (offset: number) => void;
}) {
  const Row = ({
    index,
    style,
    data,
  }: {
    index: number;
    style: CSSProperties;
    data: string[];
  }) => {
    const name = data[index];
    return (
      <div className={listStyles.row} style={style}>
        <div className={styles.itemContainer}>
          <div className={styles.item}>{name}</div>
        </div>
        <div className={listStyles.buttons}>
          <button onClick={() => onMarkFavorite(name)}>
            {isFavorite(name) ? (
              <img src="/heart.svg" />
            ) : (
              <img src="/heart-outline.svg" />
            )}
          </button>
        </div>
      </div>
    );
  };

  return (
    <div className={classNames(styles.nameList, listStyles.container)}>
      {!names || !names.length ? (
        <div className={styles.noNamesFound}>Keine Namen gefunden</div>
      ) : (
        <AutoSizer>
          {({ height, width }) => (
            <List
              height={height}
              width={width}
              itemCount={names.length}
              itemSize={50}
              itemData={names}
              initialScrollOffset={initialScrollOffset}
              onScroll={(props) => onScroll(props.scrollOffset)}
            >
              {Row}
            </List>
          )}
        </AutoSizer>
      )}
    </div>
  );
}
